import com.conygre.test.pets.Person;
import com.conygre.test.pets.PersonConfiguration;
import com.conygre.test.pets.Pet;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class PersonTest {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(PersonConfiguration.class); 
        context.getBean(Person.class).getPet().feed();
        Person personFromSpring = context.getBean(Person.class);
        Pet petFromSpring = personFromSpring.getPet();
        petFromSpring.feed();
        
    }
}
