package com.conygre.test.pets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PersonO {
    @Autowired
    private Address add;

    public PersonO(Address add){
        this.add = add;

    }

    public Address getAdd(){
        return this.add;
    }

    public void setAdd(Address add){
        this.add = add;
    }
}